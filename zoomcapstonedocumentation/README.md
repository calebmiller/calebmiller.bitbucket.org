# This site hosts the generated documentation for the UNO Zoom Capstone Project Team [Spring 2017] #

## **Zoom Capstone Team:** ##
Caleb Miller

Kris Davlin

Nishchala Tangirala

Nohemi Ramirez

## **Team Description:** ##

We are a group from the University of Nebraska-Omaha working together on our capstone project. We chose to work on a project supplied to us by BuilderTREND, a company out of Omaha that provides software solutions for construction companies. They were one of the first companies in the industry, and are still today a leading competitor.

##** Project Description: **##

The goal of the project was to research the Zoom API, and eventually create a web application that would make use of their API calls to help BuilderTREND in scheduling meetings, and recording/downloading meetings. They would like a portal for which their employees can schedule meetings, allowing them to keep track of the meeting, and download the recordings from the Zoom cloud to be stored for later use.

### **Google Drive containing additional project information/documentation:** ###

https://drive.google.com/drive/u/1/folders/0B9UUVVFp5kggUExpRlZHZUhHUFE?ths=true
(must have access to folder)

### **Trello Board for project management:** ###
https://trello.com/b/eOG7unPp/zoom-integration