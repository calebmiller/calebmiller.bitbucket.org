var indexSectionsWithContent =
{
  0: "abcdegilmnoprsuz",
  1: "abcdegsz",
  2: "z",
  3: "abcdegrsz",
  4: "abcdegilopsuz",
  5: "bcdpsz",
  6: "acdmnuz",
  7: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "properties",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Properties",
  7: "Pages"
};

